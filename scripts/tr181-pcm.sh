#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="pcm-manager"
datamodel_root="PersistentConfiguration"

#Show PCM backup files content
pcm_content(){
   for file in /cfg/pcm/*.json; do
      echo "Content of PCM backup file ${file#/cfg/pcm/}:"
      cat "$file"
   done
}

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        pcm_content
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
