MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../common)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -Wno-unused-variable -Wno-unused-parameter \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL_DEFAULT=500 \
		  -DCONFIG_SAH_AMX_PCM_TARBALL -D_FILE_OFFSET_BITS=64


LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxd -lamxo -lamxb -lamxm -lamxp\
		   -lsahtrace -larchive -lssl -lcrypto\
		   -ldl -lpthread


WRAP_FUNC=-Wl,--wrap=
MOCK_WRAP =  amxm_execute_function \
			 amxo_parser_save_object \
			 archive_write_header archive_read_data_into_fd \
			 amxb_be_who_has \
			 amxb_set \
			 pcm_tar_create pcm_tar_extract


LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
