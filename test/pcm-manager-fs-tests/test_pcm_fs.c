/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include <string.h>

#include "fs_utils.h"
#include "test_pcm_fs.h"
#include "test_helper.h"

#define TEST_ROOT_DIR       "tmp/fs_utils"
#define EXISTING_PATH       TEST_ROOT_DIR "/existing"
#define NON_EXISTING_PATH   TEST_ROOT_DIR "/not_existing"
#define SINGLE_DIR_PATH     TEST_ROOT_DIR "/single"
#define COMPLEX_DIR_PATH    TEST_ROOT_DIR "/level_a/level_b/level_c/level_d"
#define PCM_USR_DIR         TEST_ROOT_DIR "/pcm.usr"
#define TEST_FILES          PCM_USR_DIR "/backup{001..003}.json"
#define TAR_TEST_FILE       "pcm.tar"
#define TAR_TEST_FILE_PATH  PCM_USR_DIR "/" TAR_TEST_FILE

#define REMOVE_ROOT_DIR     "rm -rf "   TEST_ROOT_DIR
#define CREATE_INITIAL_TREE "mkdir -p " EXISTING_PATH
#define REMOVE_PCM_USR_DIR  "rm -rf "   PCM_USR_DIR
#define CREATE_PCM_USR_DIR  "mkdir -p " PCM_USR_DIR
#define CREATE_FILES        "touch "    TEST_FILES
#define CREATE_TAR_FILE     "touch "    TAR_TEST_FILE_PATH


int test_setup(UNUSED void** state) {
    //ensure all clean
    assert_int_equal(0, system(REMOVE_ROOT_DIR));
    assert_int_equal(0, system(CREATE_INITIAL_TREE));
    assert_true(directory_exist(TEST_ROOT_DIR));
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_int_equal(0, system(REMOVE_ROOT_DIR));
    return 0;
}

void test_directory_exist(UNUSED void** state) {
    assert_true(directory_exist(EXISTING_PATH));
}

void test_directory_not_exist(UNUSED void** state) {
    assert_false(directory_exist(NON_EXISTING_PATH));
}

void test_single_dir_create(UNUSED void** state) {
    assert_false(directory_exist(SINGLE_DIR_PATH));
    assert_int_equal(0, mkdir_p(SINGLE_DIR_PATH));
    assert_false(directory_exist(SINGLE_DIR_PATH));
}

void test_complex_path_create(UNUSED void** state) {
    assert_false(directory_exist(COMPLEX_DIR_PATH));
    assert_int_equal(0, mkdir_p(COMPLEX_DIR_PATH));
    assert_false(directory_exist(COMPLEX_DIR_PATH));
}

void test_get_files_from_path(UNUSED void** state) {
    amxc_llist_t* directories = NULL;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_int_equal(0, system(CREATE_PCM_USR_DIR));
    assert_true(directory_exist(PCM_USR_DIR));

    assert_int_equal(0, system(CREATE_FILES));
    directories = files_from_path(PCM_USR_DIR);
    assert_non_null(directories);

    assert_int_equal(amxc_llist_size(directories), 3);
    amxc_llist_for_each(iter, directories) {
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        const char* file_str = amxc_string_get(file, 0);
        assert_true(strcmp(file_str, "backup_001.json") ||
                    strcmp(file_str, "backup_002.json") ||
                    strcmp(file_str, "backup_003.json"));
    }

    amxc_llist_delete(&directories, amxc_string_list_it_free);
    directories = NULL;
}

void test_get_files_is_empty(UNUSED void** state) {
    amxc_llist_t* directories = NULL;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_int_equal(0, system(CREATE_PCM_USR_DIR));
    assert_true(directory_exist(PCM_USR_DIR));

    directories = files_from_path(PCM_USR_DIR);
    assert_null(directories);

    directories = NULL;
}

void test_cannot_tar_append_file(void** state) {
    assert_false(file_exist(TAR_TEST_FILE_PATH));
    set_tar_append_to_fail(true);
    // archive_write_header will return -1 in pcm_tar_create
    assert_false(pcm_tar_create(PCM_USR_DIR, TAR_TEST_FILE));
    assert_false(file_exist(TAR_TEST_FILE_PATH));
}

void test_can_create_tarball(UNUSED void** state) {
    amxc_llist_t* directories = NULL;

    assert_true(directory_exist(PCM_USR_DIR));
    assert_int_equal(0, system(REMOVE_PCM_USR_DIR));

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_false(pcm_tar_create(PCM_USR_DIR, TAR_TEST_FILE));

    assert_int_equal(0, system(CREATE_PCM_USR_DIR));
    assert_true(directory_exist(PCM_USR_DIR));

    assert_false(file_exist(TAR_TEST_FILE_PATH));

    assert_int_equal(0, system(CREATE_FILES));
    directories = files_from_path(PCM_USR_DIR);
    assert_non_null(directories);

    set_tar_append_to_fail(false);
    set_func_return_value(called);
    assert_true(pcm_tar_create(PCM_USR_DIR, TAR_TEST_FILE));

    amxc_llist_delete(&directories, amxc_string_list_it_free);
    directories = NULL;

    assert_true(file_exist(TAR_TEST_FILE_PATH));
    assert_false(file_exist(PCM_USR_DIR "/backup001.json"));
    assert_false(file_exist(PCM_USR_DIR "/backup002.json"));
    assert_false(file_exist(PCM_USR_DIR "/backup003.json"));
}

void test_cannot_tar_extract_file(void** state) {
    assert_true(file_exist(TAR_TEST_FILE_PATH));
    set_tar_extract_to_fail(true);
    set_func_return_value(called);
    // archive_read_data_into_fd will return -1 in pcm_tar_extract
    assert_false(pcm_tar_extract(PCM_USR_DIR, TAR_TEST_FILE));
}

void test_can_extract_tarball(UNUSED void** state) {
    amxc_llist_t* directories = NULL;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(directory_exist(PCM_USR_DIR));
    assert_true(file_exist(TAR_TEST_FILE_PATH));

    set_tar_extract_to_fail(false);
    set_func_return_value(called);
    assert_true(pcm_tar_extract(PCM_USR_DIR, TAR_TEST_FILE));

    assert_false(file_exist(TAR_TEST_FILE_PATH));
    assert_true(file_exist(PCM_USR_DIR "/backup001.json"));
    assert_true(file_exist(PCM_USR_DIR "/backup002.json"));
    assert_true(file_exist(PCM_USR_DIR "/backup003.json"));
}
