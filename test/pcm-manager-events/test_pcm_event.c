/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/stat.h>

#include "../common/test_helper.h"
#include "dm_pcm.h"
#include "dm_pcm_rpc.h"

#include "test_pcm_event.h"
#define  PCM_ROOT_OBJ "PersistentConfiguration"

#define TEST_ROOT_DIR "tmp/"
#define CREATE_TEST_DIR "mkdir -p "TEST_ROOT_DIR
#define REMOVE_TEST_DIR "rm -rf "TEST_ROOT_DIR


static pcm_test_helper test_env;

static amxd_dm_t* test_pcm_get_dm(void) {
    return test_env.dm;
}

int test_setup(UNUSED void** state) {
    assert_int_equal(0, system(CREATE_TEST_DIR));
    pcm_test_helper_setup(&test_env);
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(0, system(REMOVE_TEST_DIR));
    pcm_test_helper_teardown(&test_env);
    return 0;
}

void test_can_register(UNUSED void** state) {
    const char* const service_name = "PcmManagerTest";
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxc_var_t* data = NULL;
    amxc_var_t* info = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t command;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&command, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    info = amxc_var_add_key(amxc_htable_t, data, "info", NULL);
    amxc_var_add_key(cstring_t, info, "rpcImport", "ImportTest");
    amxc_var_add_key(cstring_t, info, "rpcExport", "ExportTest");
    amxc_var_add_key(int32_t, info, "priority", 0);
    amxc_var_add_key(cstring_t, info, "dmRoot", service_name);
    amxc_var_add_key(cstring_t, info, "version", "1.0");
    amxc_var_add_key(cstring_t, info, "name", name);
    amxc_var_add_key(bool, info, "outOfOrder", true);

    //Create a dummy backup file
    amxc_string_setf(&command, "touch %s%s.json", TEST_ROOT_DIR, name);
    assert_int_equal(0, system(amxc_string_get(&command, 0)));

    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_registerSvc(NULL, NULL, &args, &ret), amxd_status_ok);
    pcm_wait_for_timer_event();
    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&command);
}

void test_change_backup_file_type(UNUSED void** state) {
    amxd_trans_t trans;
    char* controller_str = NULL;
    amxd_object_t* pcm_obj = NULL;

    pcm_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()), PCM_ROOT_OBJ);
    assert_non_null(pcm_obj);

    //JSON -> XML
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "%s.Config.", PCM_ROOT_OBJ);
    amxd_trans_set_value(cstring_t, &trans, "BackupFileType", "XML");
    assert_int_equal(amxd_trans_apply(&trans, pcm_get_dm()), amxd_status_ok);
    pcm_handle_events();
    amxd_trans_clean(&trans);
    controller_str = amxd_object_get_cstring_t(pcm_obj, "Controller", NULL);
    assert_string_equal(controller_str, "mod-pcm-xml");
    free(controller_str);

    //XML->JSON
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "%s.Config.", PCM_ROOT_OBJ);
    amxd_trans_set_value(cstring_t, &trans, "BackupFileType", "JSON");
    assert_int_equal(amxd_trans_apply(&trans, pcm_get_dm()), amxd_status_ok);
    pcm_handle_events();
    amxd_trans_clean(&trans);
    controller_str = amxd_object_get_cstring_t(pcm_obj, "Controller", NULL);
    assert_string_equal(controller_str, "mod-pcm-json");
    free(controller_str);

    //JSON -> XML
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "%s.Config.", PCM_ROOT_OBJ);
    amxd_trans_set_value(cstring_t, &trans, "BackupFileType", "XML");
    assert_int_equal(amxd_trans_apply(&trans, pcm_get_dm()), amxd_status_ok);
    pcm_handle_events();
    amxd_trans_clean(&trans);
    controller_str = amxd_object_get_cstring_t(pcm_obj, "Controller", NULL);
    assert_string_equal(controller_str, "mod-pcm-xml");
    free(controller_str);

    //XML->JSON
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "%s.Config.", PCM_ROOT_OBJ);
    amxd_trans_set_value(cstring_t, &trans, "BackupFileType", "JSON");
    assert_int_equal(amxd_trans_apply(&trans, pcm_get_dm()), amxd_status_ok);
    pcm_handle_events();
    amxd_trans_clean(&trans);
    controller_str = amxd_object_get_cstring_t(pcm_obj, "Controller", NULL);
    assert_string_equal(controller_str, "mod-pcm-json");
    free(controller_str);

}

void test_change_max_dynamic_number(void** state) {
    amxd_object_t* pcm_obj = NULL;
    amxd_object_t* backup_file_obj = NULL;
    amxd_trans_t trans;
    amxc_var_t args;
    amxc_var_t ret;
    uint32_t max_backup_files = 0;
    uint32_t number_of_insts = 0;

    pcm_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()), PCM_ROOT_OBJ);
    assert_non_null(pcm_obj);

    backup_file_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()), "%s.BackupFile.", PCM_ROOT_OBJ);
    assert_non_null(pcm_obj);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "FileName", "test_file");
    amxc_var_add_key(cstring_t, &args, "Tag", "Dynamic");

    //Fill up the BackupFile object with backupfile instances
    max_backup_files = amxd_object_get_value(uint32_t, pcm_obj, "MaxNumberOfDynamicFiles", NULL);

    for(size_t i = 0; i < max_backup_files; i++) {
        assert_int_equal(_AddBackupFile(NULL, NULL, &args, &ret), amxd_status_ok);
        pcm_wait_for_timer_event();
    }

    number_of_insts = amxd_object_get_instance_count(backup_file_obj);
    assert_int_equal(number_of_insts, max_backup_files);

    //Reduce the MaxNumberOfDynamicFiles
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, pcm_obj);
    amxd_trans_set_value(uint32_t, &trans, "MaxNumberOfDynamicFiles", 4);
    assert_int_equal(amxd_trans_apply(&trans, pcm_get_dm()), amxd_status_ok);
    pcm_handle_events();
    amxd_trans_clean(&trans);

    max_backup_files = amxd_object_get_value(uint32_t, pcm_obj, "MaxNumberOfDynamicFiles", NULL);
    assert_int_equal(max_backup_files, 4);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}