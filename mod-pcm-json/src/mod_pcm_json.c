/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <stdio.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>


#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxc/amxc_variant_type.h>

#include <amxj/amxj_variant.h>

#include <fcntl.h>

#include "mod_pcm_json.h"

#ifdef ME
#undef ME
#endif

#define ME "pcm_json"

typedef struct  {
    amxm_shared_object_t* so;
} mod_pcm_json_t;

static mod_pcm_json_t pcm;
static amxd_status_t pcm_write_variant(const char* out_file, const amxc_var_t* const content);
static amxd_status_t pcm_read_variant(const char* in_file, amxc_var_t* const content);

int pcm_store_json(UNUSED const char* function_name,
                   amxc_var_t* args,
                   UNUSED amxc_var_t* ret) {
    int rc = amxd_status_unknown_error;
    const char* filename = NULL;
    const amxc_var_t* content = NULL;

    when_null(args, exit);

    filename = GETP_CHAR(args, "name");
    content = GETP_ARG(args, "content");

    when_str_empty(filename, exit);
    when_null(content, exit);

    SAH_TRACEZ_INFO(ME, "Store backup %s", filename);
    rc = pcm_write_variant(filename, content);

exit:
    return rc;
}

int pcm_load_json(UNUSED const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret) {
    int rc = amxd_status_unknown_error;
    const char* filename = NULL;

    when_null(args, exit);
    when_null(ret, exit);

    filename = GETP_CHAR(args, "name");
    when_str_empty(filename, exit);

    SAH_TRACEZ_INFO(ME, "Read backup %s", filename);
    rc = pcm_read_variant(filename, ret);

exit:
    return rc;
}

static AMXM_CONSTRUCTOR pcm_json_start(void) {
    amxm_module_t* mod = NULL;
    pcm.so = amxm_so_get_current();

    amxm_module_register(&mod, pcm.so, MOD_PCM_JSON);
    amxm_module_add_function(mod, "load-backup", pcm_load_json);
    amxm_module_add_function(mod, "store-backup", pcm_store_json);

    return 0;
}

static AMXM_DESTRUCTOR pcm_json_stop(void) {
    return 0;
}


static amxd_status_t pcm_write_variant(const char* out_file, const amxc_var_t* const content) {
    amxd_status_t rc = amxd_status_unknown_error;
    variant_json_t* writer = NULL;
    int fd = open(out_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    when_true(-1 == fd, exit);
    when_failed(amxj_writer_new(&writer, content), exit);

    rc = (0 != amxj_write(writer, fd)) ? amxd_status_ok : amxd_status_unknown_error;
    close(fd);

exit:
    amxj_writer_delete(&writer);
    return rc;
}

static amxd_status_t pcm_read_variant(const char* in_file, amxc_var_t* const content) {
    amxd_status_t rc = amxd_status_unknown_error;
    variant_json_t* reader = NULL;
    int fd = open(in_file, O_RDONLY, 0644);
    amxc_var_t* var = NULL;
    amxc_var_t* data = NULL;
    int read_length = 0;

    amxj_reader_new(&reader);
    when_true(-1 == fd, exit);

    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);
    when_null(var, exit);

    data = amxc_var_add_key(amxc_htable_t, content, "data", NULL);
    rc = amxc_var_copy(data, var);

    if(amxd_status_ok != rc) {
        SAH_TRACEZ_ERROR(ME, "Copy backup data to variant error %s", amxd_status_string(rc));
    }

    close(fd);

exit:
    amxj_reader_delete(&reader);
    amxc_var_delete(&var);
    return rc;
}


#undef ME