/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include <amxm/amxm.h>
#include <string.h>
#include <time.h>
#include "dm_pcm.h"
#include "dm_pcm_mngr.h"
#include "dm_pcm_service.h"
#include "fs_utils.h"

#define PCM_ROOT_OBJ_BASE "PersistentConfiguration"
#define ME "pcm"

static pcm_app_t pcm;
static amxc_llist_t scheduled_backups;

static void pcm_backup_scheduled_services(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxc_llist_for_each(iter, &scheduled_backups) {
        amxc_string_t* service = amxc_container_of(iter, amxc_string_t, it);
        amxc_llist_it_take(iter);

        if(NULL != service) {
            SAH_TRACEZ_INFO(ME, "Backup service %s", amxc_string_get(service, 0));
            amxd_object_t* service_object = pcm_get_service(amxc_string_get(service, 0));
            (void) pcm_backup_service(service_object, "upc", "upc, usersetting");
        }
        amxc_string_list_it_free(iter);
    }
}

static int pcm_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int ret = -1;
    pcm.dm = dm;
    pcm.parser = parser;
    amxc_string_new(&pcm.root_object, 0);
    amxc_string_setf(pcm.root_object, "%s", PCM_ROOT_OBJ_BASE);

    if(!directory_exist(pcm_get_store_path("upc"))) {
        (void) mkdir_p(pcm_get_store_path("upc"));
    }

    if(!directory_exist(pcm_get_store_path("export"))) {
        (void) mkdir_p(pcm_get_store_path("export"));
    }

    if(!directory_exist(pcm_get_store_path("reset"))) {
        (void) mkdir_p(pcm_get_store_path("reset"));
    }

    amxc_llist_init(&scheduled_backups);
    ret = amxp_timer_new(&pcm.scheduler, pcm_backup_scheduled_services, NULL);
    amxp_timer_set_interval(pcm.scheduler, CHECK_SCHEDULED_SERVICES);
    amxp_timer_start(pcm.scheduler, CHECK_SCHEDULED_SERVICES);

    return ret;
}

static void pcm_cleanup(void) {
    amxc_string_delete(&pcm.root_object);
    amxp_timer_stop(pcm.scheduler);
    amxp_timer_delete(&pcm.scheduler);
    amxc_llist_clean(&scheduled_backups, amxc_string_list_it_free);
    pcm.dm = NULL;
    pcm.parser = NULL;
}

const char* pcm_root_object(void) {
    return amxc_string_get(pcm.root_object, 0);
}

amxd_dm_t* pcm_get_dm(void) {
    return pcm.dm;
}

amxo_parser_t* PRIVATE pcm_get_parser(void) {
    return pcm.parser;
}

const char* PRIVATE pcm_get_store_path(const char* type) {
    const char* retval = NULL;
    amxc_var_t* path_var = NULL;

    when_str_empty_trace(type, exit, ERROR, "backup type is not provided");

    if(strcmp(type, "upc") == 0) {
        path_var = amxo_parser_get_config(pcm_get_parser(), "upc_backup_path");
    } else if(strcmp(type, "export") == 0) {
        path_var = amxo_parser_get_config(pcm_get_parser(), "export_backup_path");
    } else if(strcmp(type, "reset") == 0) {
        path_var = amxo_parser_get_config(pcm_get_parser(), "reset_backup_path");
    } else {
        SAH_TRACEZ_ERROR(ME, "Backup type: %s is not supported", type);
        goto exit;
    }

    retval = amxc_var_constcast(cstring_t, path_var);
exit:
    return retval;
}

const char* PRIVATE pcm_get_modules_path(void) {
    amxc_var_t* setting = amxo_parser_get_config(pcm_get_parser(), "pcm_modules_path");
    return amxc_var_constcast(cstring_t, setting);
}

amxc_llist_it_t* PRIVATE add_scheduler_string(const char* text) {
    return amxc_llist_add_string(&scheduled_backups, text);
}

const char* PRIVATE pcm_get_ctrl(void) {
    amxd_object_t* instance = amxd_dm_findf(pcm_get_dm(), "%s.", pcm_root_object());
    const amxc_var_t* pmc_ctrl = amxd_object_get_param_value(instance, PCM_CTRL);

    return GET_CHAR(pmc_ctrl, NULL);
}

amxc_string_t* pcm_get_backup_file(const char* name) {
    amxc_string_t* retval = NULL;

    amxc_string_new(&retval, 0);

    switch(pcm_get_file_type()) {
    case pcm_filetype_json:
        amxc_string_setf(retval, "%s.json", name);
        break;
    case pcm_filetype_xml:
        amxc_string_setf(retval, "%s.xml", name);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Invalid file type");
    }

    return retval;
}


void PRIVATE pcm_backup_file_path(const char* service, amxc_var_t* store_args, const char* backup_type) {
    amxc_string_t path;
    amxc_string_t* file_name = NULL;

    amxc_string_init(&path, 0);
    when_str_empty_trace(service, exit, ERROR, "Service name is empty");
    when_null(store_args, exit);

    file_name = pcm_get_backup_file(service);
    amxc_string_setf(&path, "%s/%s", pcm_get_store_path(backup_type), amxc_string_get(file_name, 0));
    amxc_var_add_key(cstring_t, store_args, "name", amxc_string_get(&path, 0));
    amxc_string_delete(&file_name);

exit:
    amxc_string_clean(&path);
    return;
}

amxd_object_t* PRIVATE pcm_get_service(const char* name) {
    amxd_object_t* service = NULL;
    amxd_object_t* services = amxd_dm_findf(pcm_get_dm(), "%s.Service.", pcm_root_object());

    when_str_empty(name, exit);
    when_null(services, exit);

    amxd_object_for_each(instance, it, services) {
        amxd_object_t* svc = amxc_container_of(it, amxd_object_t, it);
        char* alias = amxd_object_get_cstring_t(svc, "Alias", NULL);
        if((NULL != alias) && (0 == strcmp(alias, name))) {
            service = svc;
            free(alias);
            goto exit;
        }
        free(alias);
    }

exit:
    return service;
}

pcm_filetype_t PRIVATE pcm_get_file_type(void) {
    pcm_filetype_t ret = pcm_filetype_json;
    amxd_object_t* config = amxd_dm_findf(pcm_get_dm(), "%s.Config", pcm_root_object());
    char* file_type_str = NULL;

    when_null(config, exit);

    file_type_str = amxd_object_get_cstring_t(config, "BackupFileType", NULL);
    when_str_empty(file_type_str, exit);
    SAH_TRACEZ_INFO(ME, "File format of backup files is %s", file_type_str);
    if(strcmp("JSON", file_type_str) == 0) {
        ret = pcm_filetype_json;
        goto exit;
    }

    if(strcmp("XML", file_type_str) == 0) {
        ret = pcm_filetype_xml;
        goto exit;
    }

exit:
    free(file_type_str);
    return ret;
}

int _pcm_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    switch(reason) {
    case AMXO_START:
        SAH_TRACEZ_INFO(ME, "Persistent Configuration Manager started");
        pcm_init(dm, parser);
        pcm_dm_mngr_init();
        break;
    case AMXO_STOP:
        SAH_TRACEZ_INFO(ME, "Persistent Configuration Manager stopped");
        pcm_cleanup();
        amxm_close_all();
        break;
    }

    return 0;
}

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    status = amxd_action_param_check_is_in(object, param, reason,
                                           args, retval, priv);

    if(status != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            status = amxd_status_ok;
        }
        free(input);
    }

    return status;
}

#undef ME
