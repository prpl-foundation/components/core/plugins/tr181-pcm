/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dm_pcm.h"
#include "fs_utils.h"
#include "dm_pcm_service.h"
#include "dm_pcm_rpc.h"

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxd/amxd_transaction.h>

#define ME "pcm"

static amxd_status_t pcm_fill_object_parameters(const char* mode, amxd_trans_t* trans, const amxc_var_t* parameters) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* order = NULL;

    when_str_empty(mode, exit);
    when_null(trans, exit);

    when_failed(amxd_trans_set_cstring_t(trans, "ServiceStatus", mode), exit);
    rc = amxd_status_ok;
    if(NULL != parameters) {
        rc &= amxd_trans_set_param(trans, "ImportCallback",
                                   GETP_ARG(parameters, "rpcImport"));
        rc &= amxd_trans_set_param(trans, "ExportCallback",
                                   GETP_ARG(parameters, "rpcExport"));
        rc &= amxd_trans_set_param(trans, "Name",
                                   GETP_ARG(parameters, "dmRoot"));
        order = GETP_ARG(parameters, "priority");
        if(NULL != order) {
            rc &= amxd_trans_set_param(trans, "Order", order);
        }
    }

exit:
    return rc;
}

static amxd_status_t pcm_add_instance(const char* service, const amxc_var_t* parameters) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_trans_init(&trans);

    when_str_empty(service, exit);

    status = amxd_trans_select_pathf(&trans, "%s.Service.", pcm_root_object());
    when_failed(status, exit);

    when_failed(amxd_trans_add_inst(&trans, 0, service), exit);
    when_failed(pcm_fill_object_parameters("Online", &trans, parameters), exit);

    status = amxd_trans_apply(&trans, pcm_get_dm());

exit:
    amxd_trans_clean(&trans);
    return status;
}

static amxd_status_t pcm_update_instance(const char* service, const char* mode, const amxc_var_t* parameters) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_trans_init(&trans);
    amxd_object_t* object = NULL;

    when_str_empty(service, exit);

    object = amxd_dm_findf(pcm_get_dm(), "%s.Service.%s", pcm_root_object(), service);
    when_null(object, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    status = amxd_trans_select_object(&trans, object);
    when_failed(status, exit);

    status = pcm_fill_object_parameters(mode, &trans, parameters);
    when_failed(status, exit);

    status = amxd_trans_apply(&trans, pcm_get_dm());

exit:
    amxd_trans_clean(&trans);
    return status;
}

static bool pcm_service_exist(const char* service) {
    bool rc = false;
    amxd_object_t* instance = NULL;

    when_str_empty(service, exit);

    instance = amxd_dm_findf(pcm_get_dm(), "%s.Service.%s", pcm_root_object(), service);
    rc = NULL != instance;

exit:
    return rc;
}

static bool pcm_should_restore(const char* name) {
    amxd_object_t* service = pcm_get_service(name);
    bool import_on_register = false;
    char* import_status = NULL;
    when_null(service, exit);
    import_status = amxd_object_get_cstring_t(service, "ImportStatus", NULL);

    when_str_empty(import_status, exit);

    SAH_TRACEZ_INFO(ME, "ImportStatus == %s", import_status);

    if(strcmp("None", import_status) == 0) {
        import_on_register = true;
    }

exit:
    free(import_status);
    return import_on_register;
}

static pcm_auto_sync_t pcm_auto_sync_from_string(const char* mode) {
    when_str_empty(mode, exit);

    if(strcmp("Enable", mode) == 0) {
        return Enable;
    }

    if(strcmp("ForceEnable", mode) == 0) {
        return ForceEnable;
    }
exit:
    return Disable;
}

static pcm_auto_sync_t pcm_get_auto_sync_mode(void) {
    amxd_object_t* config = amxd_dm_findf(pcm_get_dm(), "%s.Config", pcm_root_object());
    pcm_auto_sync_t mode = Disable;
    char* mode_str = NULL;

    when_null(config, exit);

    mode_str = amxd_object_get_cstring_t(config, "AutoSync", NULL);
    when_str_empty(mode_str, exit);

    SAH_TRACEZ_INFO(ME, "AutoSync import mode %s", mode_str);

    mode = pcm_auto_sync_from_string(mode_str);

exit:
    free(mode_str);
    return mode;
}

static void set_state_of(const char* parameter, const char* state) {
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    when_str_empty_trace(parameter, exit, ERROR, "Invalid argument");
    when_str_empty_trace(state, exit, ERROR, "Invalid argument");

    amxd_trans_select_pathf(&transaction, "%s", pcm_root_object());
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &transaction, parameter, state);
    if(amxd_status_ok != amxd_trans_apply(&transaction, pcm_get_dm())) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction %s -> %s", parameter, state);
    }

exit:
    amxd_trans_clean(&transaction);
    return;
}

amxd_status_t _registerSvc(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {

    amxd_status_t status = amxd_status_unknown_error;
    const cstring_t name = NULL;
    amxc_var_t* data = NULL;
    amxc_var_t* info = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    when_null(args, exit);

    name = GET_CHAR(args, "name");
    data = GET_ARG(args, "data");
    info = GET_ARG(data, "info");

    if(pcm_service_exist(name)) {
        status = pcm_update_instance(name, "Online", info);
        SAH_TRACEZ_INFO(ME, "%s is back Online", name);
    } else {
        when_failed_trace(pcm_add_instance(name, info), exit, ERROR, "Failed to add a new instance");
        SAH_TRACEZ_INFO(ME, "Register a new service [%s]", name);
        status = amxd_status_ok;
    }

    if(pcm_should_restore(name)) {
        amxd_status_t restore_status = amxd_status_file_not_found;
        const char* alter_backup = GET_CHAR(info, "backup_file");
        amxc_string_t* file_name = NULL;

        restore_status = pcm_load_backup(name, ret, "upc");
        if(restore_status == amxd_status_ok) {
            SAH_TRACEZ_INFO(ME, "Restore on Register [%s] with UPC backup file", name);
            file_name = pcm_get_backup_file(name);
            remove_file(pcm_get_store_path("upc"), amxc_string_get(file_name, 0));
            amxc_string_delete(&file_name);
            goto exit;
        }

        restore_status = pcm_load_backup(name, ret, "reset");
        if(restore_status == amxd_status_ok) {
            SAH_TRACEZ_INFO(ME, "Restore on Register [%s] with RESET backup file", name);
            file_name = pcm_get_backup_file(name);
            remove_file(pcm_get_store_path("reset"), amxc_string_get(file_name, 0));
            amxc_string_delete(&file_name);
            goto exit;
        }

        restore_status = pcm_load_backup(alter_backup, ret, "upc");
        if(restore_status == amxd_status_ok) {
            SAH_TRACEZ_INFO(ME, "Restore on Register [%s] with Alternative backup file", name);
            file_name = pcm_get_backup_file(alter_backup);
            remove_file(pcm_get_store_path("upc"), amxc_string_get(file_name, 0));
            amxc_string_delete(&file_name);
            goto exit;
        }
    }

exit:
    return status;
}

amxd_status_t _unregisterSvc(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {

    const char* name = GET_CHAR(args, "name");
    amxd_status_t status = amxd_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "Unregister service %s", name);
    status = pcm_update_instance(name, "Offline", NULL);
    return status;
}

amxd_status_t _ServiceConfigurationChanged(UNUSED amxd_object_t* object,
                                           UNUSED amxd_function_t* func,
                                           amxc_var_t* args,
                                           UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* name = GET_CHAR(args, "name");

    pcm_auto_sync_t mode = pcm_get_auto_sync_mode();
    if(mode == Disable) {
        status = amxd_status_ok;
    } else {
        when_str_empty(name, exit);
        SAH_TRACEZ_INFO(ME, "Configuration changed for %s. Schedule import", name);
        status = (NULL != add_scheduler_string(name)) ? amxd_status_ok : amxd_status_unknown_error;
    }
exit:

    return status;
}

/**
 *  Example implementation of custom Import which could overwrite one from mod_pcm_svc
 *  Preferably this function as Export should be placed in separate module
 *  but for having simple example was put directly in plugin *
 */
amxd_status_t _Import(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxd_trans_t trans;
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* content = NULL;
    amxc_var_t* security = NULL;

    amxd_trans_init(&trans);
    when_null(args, exit);
    content = GETP_ARG(args, "content.upc");
    security = amxc_var_get_key(content,
                                "PersistentConfiguration.Config.Security.",
                                AMXC_VAR_FLAG_DEFAULT);
    when_null(security, exit);

    when_failed(amxd_trans_select_pathf(&trans, "%s.Config.Security.", pcm_root_object()), exit);
    when_failed(amxd_trans_set_param(&trans, "EncryptUserFile", GETP_ARG(security, "EncryptUserFile")), exit);

    rc = amxd_trans_apply(&trans, pcm_get_dm());
    if(amxd_status_ok != rc) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction");
    }

exit:
    amxd_trans_clean(&trans);
    return rc;
}

amxd_status_t _Backup(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_object_t* services = NULL;
    uint16_t executed_func = 0;
    uint16_t failed_func = 0;
    const char* state = "Error";
    const char* tag = NULL;
    const char* type = NULL;
    amxc_string_t userflags;
    amxc_var_t values;
    amxc_string_t filename;

    amxc_string_init(&filename, 0);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&userflags, 0);
    set_state_of("BackupStatus", "OnGoing");

    when_null_trace(args, exit, ERROR, "Invalid arguments");
    services = amxd_dm_findf(pcm_get_dm(), "%s.Service.", pcm_root_object());
    when_null_trace(services, exit, ERROR, "Could not find object %s.Service.", pcm_root_object());

    type = GET_CHAR(args, "Type");
    type = (STR_EMPTY(type))? "upc": type;

    tag = GET_CHAR(args, "Tag");
    tag = (STR_EMPTY(tag))? "Manual": tag;

    if(strcmp(type, "upc") == 0) {
        amxc_string_set(&userflags, "upc,usersetting");
    } else if(strcmp(type, "export") == 0) {
        amxc_string_set(&userflags, "usersetting");
    }

    if(!(STR_EMPTY(GET_CHAR(args, "Flags")))) {
        if(STR_EMPTY(amxc_string_get(&userflags, 0))) {
            amxc_string_appendf(&userflags, "%s", GET_CHAR(args, "Flags"));
        } else {
            amxc_string_appendf(&userflags, ",%s", GET_CHAR(args, "Flags"));
        }
    }

    amxd_object_for_each(instance, it, services) {
        amxd_object_t* service = amxc_container_of(it, amxd_object_t, it);
        failed_func += pcm_backup_service(service, type, amxc_string_get(&userflags, 0));
        executed_func++;
    }

    if(strcmp(type, "export") == 0) {
        when_false_trace(prepare_exported_backup(ret, tag), exit, ERROR, "Preparing the exported backup failed");

        if(strcmp(tag, "Manual") == 0) {
            amxb_bus_ctx_t* ctx = amxb_be_who_has("DeviceInfo.");
            when_null_trace(ctx, exit, ERROR, "Failed to get DeviceInfo. bus ctx");
            amxc_string_setf(&filename, "/tmp/pcm.usr/%s", GET_CHAR(ret, "FileName"));

            amxc_var_add_key(cstring_t, &values, "Name", amxc_string_get(&filename, 0));
            rc = amxb_set(ctx, "DeviceInfo.VendorConfigFile.[Alias=='userSettings'].", &values, NULL, 5);
            when_failed_trace(rc, exit, ERROR, "Failed to update the DeviceInfo.VendorConfigFile.[Alias=='userSettings'] instance");
        }
    }

    rc = amxd_status_ok;
    state = (failed_func == 0)? "Success": ((failed_func < executed_func)? "PartialSuccess": "Error");
    SAH_TRACEZ_INFO(ME, "The backup method completed with Status: %s", state);

exit:
    amxc_var_clean(&values);
    amxc_string_clean(&userflags);
    amxc_string_clean(&filename);
    set_state_of("BackupStatus", state);
    return rc;
}

amxd_status_t _Restore(UNUSED amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_object_t* services = NULL;
    uint16_t executed_func = 0;
    uint16_t failed_func = 0;
    const char* state = "Error";
    const char* type = NULL;
    const char* file_ref = NULL;

    set_state_of("RestoreStatus", "OnGoing");

    when_null_trace(args, exit, ERROR, "Invalid arguments");
    services = amxd_dm_findf(pcm_get_dm(), "%s.Service.", pcm_root_object());
    when_null_trace(services, exit, ERROR, "Could not find object %s.Service.", pcm_root_object());

    type = GET_CHAR(args, "Type");
    type = (STR_EMPTY(type))? "upc": type;

    file_ref = GET_CHAR(args, "FileRef");
    if(strcmp(type, "export") == 0) {
        when_str_empty_trace(file_ref, exit, ERROR, "File_Ref is not provided for the export type");
        when_false_trace(prepare_exported_restore(file_ref), exit, ERROR, "Preparing to restore the exported backup failed [%s]", file_ref);
    }

    amxd_object_for_each(instance, it, services) {
        amxd_object_t* service = amxc_container_of(it, amxd_object_t, it);
        failed_func += pcm_restore_service(service, type);
        executed_func++;
    }

    rc = amxd_status_ok;
    state = (failed_func == 0)? "Success": ((failed_func < executed_func)? "PartialSuccess": "Error");
    SAH_TRACEZ_INFO(ME, "The Restore method completed with Status: %s", state);

exit:
    set_state_of("RestoreStatus", state);
    return rc;
}

amxd_status_t _SetActionStatus(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_unknown_error;
    const char* service_name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    const char* action_status = GET_CHAR(args, "ActionStatus");
    const char* state = GET_CHAR(args, "State");

    if(STR_EMPTY(service_name) || STR_EMPTY(action_status) || STR_EMPTY(state)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        goto exit;
    }

    rc = pcm_mark_service_action(service_name, action_status, state);

exit:
    return rc;
}

amxd_status_t _AddBackupFile(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_invalid_arg;
    const char* filename = GET_CHAR(args, "FileName");
    const char* tag = GET_CHAR(args, "Tag");
    amxc_ts_t now;
    amxc_string_t* date_filename = NULL;

    when_str_empty_trace(filename, exit, ERROR, "FileName is not provided");
    when_str_empty_trace(tag, exit, ERROR, "Tag is not provided");

    amxc_ts_now(&now);
    date_filename = date_prefix_to_file(filename, &now);
    rc = amxd_status_unknown_error;
    when_null_trace(date_filename, exit, ERROR, "Could not add date prefix to the %s", filename);

    rc = pcm_add_backup_file(amxc_string_get(date_filename, 0), tag, &now, ret);
exit:
    amxc_string_delete(&date_filename);
    return rc;
}

#undef ME