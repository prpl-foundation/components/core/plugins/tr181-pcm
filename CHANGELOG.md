# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.19.8 - 2024-09-25(14:27:06 +0000)

### Other

- Update pcm-manager script

## Release v0.19.7 - 2024-09-10(07:09:01 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.19.6 - 2024-07-23(07:40:00 +0000)

### Fixes

- Better shutdown script

## Release v0.19.5 - 2024-07-18(12:54:08 +0000)

### Other

- [tr181-deviceinfo] missing VendorConfigFile path for upload

## Release v0.19.4 - 2024-07-08(13:58:07 +0000)

## Release v0.19.3 - 2024-07-08(08:40:42 +0000)

### Other

- TR-181: Device.PersistentConfiguration data model issues 19.03.2024

## Release v0.19.2 - 2024-07-01(10:04:28 +0000)

### Fixes

- [tr181-pcm] cannot execute service protected import and export functions

## Release v0.19.1 - 2024-06-25(08:16:30 +0000)

### Other

- [DeviceInfo] VendorConfigFile instance must be created when we call the PCM.Backup

## Release v0.19.0 - 2024-04-16(11:01:38 +0000)

### New

- [tr181-pcm] Userflags must be configurable

## Release v0.18.1 - 2024-04-11(09:18:22 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.18.0 - 2024-03-23(13:18:55 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.17.2 - 2024-03-08(15:02:34 +0000)

### Other

- [pcm-manager] Add support for opensslv3

## Release v0.17.1 - 2024-03-01(10:25:22 +0000)

### Fixes

- Missing acl rule for PCM

## Release v0.17.0 - 2024-02-28(10:26:11 +0000)

### New

- [tr181-pcm] Rework the Backup/Restore API

## Release v0.16.6 - 2024-01-18(07:59:57 +0000)

### Other

- [tr181-pcm] Enable large file support when using libarchive on Linux

## Release v0.16.5 - 2024-01-15(08:07:36 +0000)

### Fixes

- [PCM] BackupFile instances are not updated

## Release v0.16.4 - 2024-01-05(15:29:34 +0000)

### Fixes

- [tr181-pcm] compilation error O_CLOEXEC undeclared

## Release v0.16.3 - 2024-01-05(10:16:06 +0000)

### Fixes

- [tr181-pcm] generate tarball using libarchive

## Release v0.16.2 - 2023-11-30(09:15:02 +0000)

### Other

- Modify libbart for PCB Components Compatibility with PCM-Manager

## Release v0.16.1 - 2023-11-08(13:12:29 +0000)

## Release v0.16.0 - 2023-10-28(07:12:26 +0000)

### New

- [TR181-pcm] Add support for different backup file types

## Release v0.15.0 - 2023-10-13(15:16:48 +0000)

### New

- [amxrt][no-root-user][capability drop]pcm-manager must be adapted to run as non-root and lmited capabilities

## Release v0.14.1 - 2023-09-07(15:21:21 +0000)

### Fixes

- [tr181-pcm] Objects are not registered with pcm-manager

## Release v0.14.0 - 2023-08-31(13:52:29 +0000)

### New

- Move datamodel prefixes to the device proxy

## Release v0.13.1 - 2023-08-31(09:14:10 +0000)

### Fixes

- [tr181-DeviceInfo] Crash of ReadUsersetting and WriteUsersetting rpcs

## Release v0.13.0 - 2023-08-02(07:46:42 +0000)

### New

- [tr181-pcm] Clean up the code of pcm-manager

## Release v0.12.0 - 2023-07-26(11:47:58 +0000)

### New

- [tr181-pcm] Store multiple Usersetting backup files

## Release v0.11.1 - 2023-07-04(15:04:26 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.11.0 - 2023-06-16(14:21:55 +0000)

### New

- [tr181-pcm] Determine when the Restore/Backup is complete for each plugin

## Release v0.10.2 - 2023-06-15(08:38:26 +0000)

### Fixes

- [tr181-pcm] Restore function has stop working

## Release v0.10.1 - 2023-05-25(09:53:58 +0000)

### Other

- - [PCM] Set /cfg as the default folder for backups.

## Release v0.10.0 - 2023-05-04(14:31:44 +0000)

### New

- [tr181-pcm] Create encrypted usersetting backup files

## Release v0.9.1 - 2023-04-28(09:16:58 +0000)

### Other

- [tr181-pcm] Generation of tarball files should be configurable

## Release v0.9.0 - 2023-04-13(12:15:41 +0000)

### New

- [tr181-pcm] Backup and Restore parameters with usersetting flag

## Release v0.8.1 - 2023-04-12(07:34:37 +0000)

### Fixes

- [PCM] Duplicate instance error when loading persistent data

## Release v0.8.0 - 2023-04-04(11:35:51 +0000)

### New

- [tr181-pcm] Include XML files in the backup and restore procedure.

## Release v0.7.1 - 2023-03-20(09:50:41 +0000)

### Changes

- [Config] enable configurable coredump generation

## Release v0.7.0 - 2023-03-10(14:09:32 +0000)

### New

- [mod-pcm-svc] Migrate the mod_bart_svc to the mod_pcm_svc module

## Release v0.6.5 - 2023-02-27(09:52:47 +0000)

### Fixes

- [PCM] modify user flag "usersettings" to "usersetting"

## Release v0.6.4 - 2023-01-10(10:00:54 +0000)

### Fixes

- [tr181-pcm] unit test teardown has error

## Release v0.6.3 - 2023-01-09(10:16:04 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.6.2 - 2022-12-09(09:30:02 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.6.1 - 2022-11-07(12:19:51 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v0.6.0 - 2022-06-16(06:46:04 +0000)

### New

- [PCM] PCM-Manager support restore on register and auto sync ctrl

## Release v0.5.5 - 2022-05-19(12:44:20 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.5.4 - 2022-04-29(15:16:24 +0000)

### Fixes

- [TR181-PCM] PCMImport fails to find variants path

## Release v0.5.3 - 2022-03-24(10:59:12 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.5.2 - 2022-02-25(11:42:52 +0000)

### Other

- Enable core dumps by default

## Release v0.5.1 - 2022-01-14(09:32:20 +0000)

### Fixes

- Use default flags
- Ensure backup path exist on boot

## Release v0.5.0 - 2021-12-22(07:18:02 +0000)

### New

- pcm manager should use mod_pcm_scv for backuping itself

## Release v0.4.1 - 2021-11-29(10:42:22 +0000)

### Other

- [CI] Disable g++ compilation
- [CI] Update dependencies

## Release v0.4.0 - 2021-11-29(09:59:46 +0000)

### New

- Implement pcm_json mod (soc)

### Fixes

- Cleanup of DM + implementation of missing functions

## Release v0.3.1 - 2021-10-28(07:24:16 +0000)

### Fixes

- Add init script

## Release v0.3.0 - 2021-10-08(14:25:17 +0000)

### New

- Add initial registration support + register pcm against itself

## Release v0.2.0 - 2021-10-08(08:21:38 +0000)

### New

- Add PersistentConfiguration datamodel

## Release v0.1.0 - 2021-09-30(12:29:53 +0000)

### New

- Add initial boilerplate code for pcm-manager component

